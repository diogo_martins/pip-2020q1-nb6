import math

def is_prime(n: int) -> bool:
    # ineficiente, gera testes desnecessarios
    if n < 2:
        return False
    
    conta_div = 2

    for i in range(2, n//2+1):
        if n % i == 0:
            conta_div = conta_div + 1
    
    if conta_div > 2:
        return False
    else:
        return True
    
def is_prime2(n: int) -> bool:
    # mais eficiente, evita testes desnecessários
    if n < 2:
        return False
    
    prime = True
    
    for i in range(2, n//2+1):
        if n % i == 0:
            prime = False
            break
    
    return prime

def is_prime3(n: int) -> bool:
    # mais eficiente ainda, evita testes desnecessários
    if n < 2:
        return False
    
    prime = True
    
    for i in range(2, int(math.sqrt(n))+1):
        if n % i == 0:
            prime = False
            break
    
    return prime
    
def main():
    # codigo principal
    while True:
        n = int(input('n: '))
        if n >= 2:
            break
        else:
            print('n deve ser maior do que 1')
    
    if is_prime3(n):
        print('Primo')
    else:
        print('Composto')

if __name__ == "__main__":
    main()
    #print('Estou executando o codigo principal do modulo')

