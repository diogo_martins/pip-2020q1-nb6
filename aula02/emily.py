#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Distância euclidiana entre dois pontos em 3D
"""

import math

x1 = float(input("x1: "))
x2 = float(input("x2: "))
x3 = float(input("x3: "))

y1 = float(input("y1: "))
y2 = float(input("y2: "))
y3 = float(input("y3: "))

distancia = math.sqrt((x1-y1)**2 + (x2-y2)**2 + (x3-y3)**2)

print(f"Distancia: {distancia}")