#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calcula quanto pagar pelo lanche considerando os descontos
"""

k = int(input("k: "))
n = int(input("n: "))
p = int(input("p: "))
q = int(input("q: "))

if n > k:
    combos = k
else:
    combos = n
    
total = k * p + n * q - (p + q) * 0.2 * combos

print(f"Total: R${total:.2f}")
