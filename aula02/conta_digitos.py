#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Conta quantos dígitos um inteiro positivo n possui
"""

def conta_digitos(n: int) -> int:
    c = 0
    if n == 0:
        return 1
    while n > 0:
        n = n // 10
        c += 1
    
    return c

n = int(input("n: "))
print(f"{n} possui {conta_digitos(n)} digito(s)")

