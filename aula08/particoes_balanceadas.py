'''
estabelece a qtde de particoes balanceadas de uma
string balanceada
'''
def conta_part_balanceadas(s: str) -> int:
    conta_l = 0
    conta_r = 0
    conta_p = 0
    
    for i in range(len(s)):
        if s[i] == 'L':
            conta_l += 1
        elif s[i] == 'R':
            conta_r += 1
        if conta_l == conta_r:
            conta_p += 1
            conta_l = 0
            conta_r = 0
            
    return conta_p

'''
gera todas as particoes balanceadas da string balanceada
recebida como entrada
'''
def lista_part_balanceadas(s: str) -> [str]:
    particoes = []
    particao = ''
    conta_l = 0
    conta_r = 0
    
    for i in range(len(s)):
        particao += s[i]
        if s[i] == 'L':
            conta_l += 1
        elif s[i] == 'R':
            conta_r += 1
        if conta_l == conta_r:
            particoes.append(particao)
            particao = ''
            conta_r = 0
            conta_l = 0
            
    return particoes
            
    

s_balanceadas = ['RLRRLLRLRL', # 4
                 'RLLLLRRRLR', # 3
                 'LLLLRRRR',   # 1
                 'RLRRRLLRLL'] # 2

for s in s_balanceadas:
    print(s, lista_part_balanceadas(s))

