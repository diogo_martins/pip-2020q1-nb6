
def soma_lista(l):
    soma = 0
    
    for i in range(len(l)):
        soma = soma + l[i]
        
    return soma


nota = float(input("Nota: "))
notas = []

while nota >= 0:
    notas.append(nota)
    nota = float(input("Nota: "))
if len(notas) > 0:
    media = soma_lista(notas) / len(notas)
    print(f'Media = {media:.3f}')


