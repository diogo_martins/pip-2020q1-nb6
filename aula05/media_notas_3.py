
soma = 0.0
contador = 0

while True:
    nota = float(input("Nota: "))
    if nota < 0:
        break
    soma = soma + nota
    contador = contador + 1
if contador > 0:
    media = soma / contador
    print(f'Media = {media:.3f}')