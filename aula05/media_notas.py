'''
Calcula a media de uma quantidade indefinida de
notas
A leitura para ao digitar uma nota negativa
'''

nota = float(input("Nota: "))
soma = 0.0
contador = 0

while nota >= 0:
    soma = soma + nota
    contador = contador + 1
    nota = float(input("Nota: "))
if contador > 0:
    media = soma / contador
    print(f'Media = {media:.3f}')

