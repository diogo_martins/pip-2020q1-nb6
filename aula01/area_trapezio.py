#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calcula a area do trapezio dadas as duas bases e a altura
"""

base_a = float(input("Base A (cm): "))
base_b = float(input("Base B (cm): "))
altura = float(input("Altura (cm): "))

area = (base_a + base_b) * altura / 2

print(f"Area: {area} cm**2")

