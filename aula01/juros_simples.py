#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calcula juros simples dados o capital, a taxa e o tempo
Supoe que a taxa eh mensal
"""

capital = float(input("Capital (R$): "))
taxa = float(input("Taxa (% a.m.): "))
tempo = float(input("Tempo (meses): "))

juros = capital * taxa / 100 * tempo
total = capital + juros

print(f"Juros (R$): {juros}")
print(f"Total a pagar (R$): {total}")