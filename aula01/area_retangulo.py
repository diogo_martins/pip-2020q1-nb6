#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Calcula a area de um retangulo dados os lados
"""

lado_a = float(input("Lado A (cm):"))
lado_b = float(input("Lado B (cm): "))
area = lado_a * lado_b

print(f"Area: {area} cm**2")


