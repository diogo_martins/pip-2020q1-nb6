#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Avalia polinomio quadratico dados os coeficientes, via Bhaskara
Assume polinomios com duas raizes reais
"""

import math

print("Formato: a*x**2 + b*x + c")
a = float(input("a: "))
b = float(input("b: "))
c = float(input("c: "))

delta = b * b - 4 * a * c
delta_sqrt = math.sqrt(delta)
x_1 = (-b + delta_sqrt) / (2 * a)
x_2 = (-b - delta_sqrt) / (2 * a)

print(f"Raiz 1: {x_1}")
print(f"Raiz 2: {x_2}")

